<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Token;
use App\Models\User;
use Upwork\API\Config;
use Upwork\API\Client;
use Upwork\API\Routers\Messages;

class SendNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=' . config('services')['cloud_messanging']['server_key'],
        ];

        $messages = $this->getMessages();

        if ($messages) {
            foreach ($messages as $message) {
                $tokens = Token::get();
                foreach ($tokens as $token) {
                    $request_body = [
                        'to' => $token->token,
                        'notification' => [
                            'title' => 'Upwork',
                            'body' => $message,
                            'icon' => 'https://www.upwork.com/favicon.ico',
                        ],
                    ];
                    $fields = json_encode($request_body);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $response = curl_exec($ch);
                    curl_close($ch);

                    echo $response;
                }
            }
        }
    }

    public function getMessages()
    {
        $users = User::get();
        foreach ($users as $user) {
            $client = $this->restartClient($user->access_token, $user->access_secret);

            $messages = new Messages($client);
            $params = array(
                'limit' => 10,
            );

            for ($i = 0; $i < 3; $i++) {
                $rooms = $messages->getRooms($user->user_id, $params);
                if (!empty($rooms) && !isset($rooms->error)) {
                    foreach ($rooms->rooms as $room) {
                        if ($room->numUnread) {
                            $messagesArray[] = "User '$user->user_id' - $room->numUnread unread message(s) in room '$room->roomName' with topic '$room->topic'";
                        }
                    }
                    break;
                }
            }
        }

        return $messagesArray ?? null;
    }

    public function restartClient($accessToken, $accessSecret)
    {
        $config = new Config([
            'consumerKey'       => config('services')['upwork']['consumer_key'],
            'consumerSecret'    => config('services')['upwork']['consumer_secret'],
            'accessToken'       => $accessToken,
            'accessSecret'      => $accessSecret,
        ]);

        return new Client($config);
    }
}
