<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $primaryKey = 'reference';
    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'reference',
        'user_id',
        'first_name',
        'last_name',
        'email',
        'public_url',
        'access_token',
        'access_secret',
    ];
}
