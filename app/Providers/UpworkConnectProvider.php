<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Upwork\API\Client;
use Upwork\API\Config;

class UpworkConnectProvider extends ServiceProvider
{
    public function register()
    {
        session_start();

        //$this->app->bind(Client::class, function ($app) {
        $this->app->singleton(Client::class, function ($app) {

            $config = new Config([
                    'consumerKey'       => config('services')['upwork']['consumer_key'],
                    'consumerSecret'    => config('services')['upwork']['consumer_secret'],

                    'accessToken'       => $_SESSION['access_token'] ?? '',
                    'accessSecret'      => $_SESSION['access_secret'] ?? '',
                    'requestToken'      => $_SESSION['request_token'] ?? '',
                    'requestSecret'     => $_SESSION['request_secret'] ?? '',
                    'verifier'          => $_GET['oauth_verifier'] ?? '',
                    'mode'              => 'web',
                    //'verifySsl'         => false,
            ]);

            $client = new Client($config);

            if (empty($_SESSION['request_token']) && empty($_SESSION['access_token'])) {
                $requestTokenInfo = $client->getRequestToken();
                $_SESSION['request_token']  = $requestTokenInfo['oauth_token'];
                $_SESSION['request_secret'] = $requestTokenInfo['oauth_token_secret'];
                $client->auth();
                exit;
            } elseif (empty($_SESSION['access_token'])) {
                $accessTokenInfo = $client->auth();
                $_SESSION['access_token']   = $accessTokenInfo['access_token'];
                $_SESSION['access_secret']  = $accessTokenInfo['access_secret'];
            }
            if (!empty($_SESSION['access_token'])) {
                unset($_SESSION['request_token']);
                unset($_SESSION['request_secret']);
            }

            return $client;
        });
    }
}
