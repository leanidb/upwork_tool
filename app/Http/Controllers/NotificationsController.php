<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Token;
use App\Models\User;
use Upwork\API\Config;
use Upwork\API\Client;
use Upwork\API\Routers\Messages;

class NotificationsController extends BaseController
{
    public function clearSession()
    {
        foreach ($_SESSION as $k => $v) {
            unset($_SESSION[$k]);
        }

        return view('welcome', [
            'userMessage' => "Session clear! Need to log out from account on <a href=\"https://www.upwork.com\" target=\"_blank\">https://www.upwork.com</a>",
        ]);
    }

    public function getUsers()
    {
        $users = User::orderBy('user_id')->get();
        foreach ($users as $user) {
            $usersArray[] = $user->user_id;
        }

        return view('welcome', [
            'users' => $usersArray ?? null,
        ]);
    }

    public function getMessages()
    {
        if (isset($_SESSION['access_token']) && isset($_SESSION['access_secret'])) {
            $users = User::get();
            foreach ($users as $user) {
                $client = $this->restartClient($user->access_token, $user->access_secret);

                $messages = new Messages($client);
                $params = array(
                    'limit' => 10,
                );

                for ($i = 0; $i < 3; $i++) {
                    $rooms = $messages->getRooms($user->user_id, $params);
                    if (!empty($rooms) && !isset($rooms->error)) {
                        break;
                    }
                }

                if (!empty($rooms) && !isset($rooms->error)) {
                    foreach ($rooms->rooms as $room) {
                        if ($room->numUnread) {
                            $messagesArray[] = [
                                'user' => $user->user_id,
                                'room_name' => $room->roomName,
                                'topic' => $room->topic,
                                'num_unread' => $room->numUnread,
                            ];
                        }
                    }
                } else {
                    $errorsArray[] = "Information about user '$user->user_id' rejected by Upwork!";
                }
            }
        } else {
            $errorsArray[] = 'Need to log in into account';
        }

        return view('welcome', [
            'messages' => $messagesArray ?? null,
            'errors' => $errorsArray ?? null,
        ]);
    }

    public function restartClient($accessToken, $accessSecret)
    {
        $config = new Config([
            'consumerKey'       => config('services')['upwork']['consumer_key'],
            'consumerSecret'    => config('services')['upwork']['consumer_secret'],
            'accessToken'       => $accessToken,
            'accessSecret'      => $accessSecret,
        ]);

        return new Client($config);
    }

    public function saveToken(Request $request)
    {
        $token = $request->get('token');
        Token::create([
            'token' => $token,
        ]);
    }
}
