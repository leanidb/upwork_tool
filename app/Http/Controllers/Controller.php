<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Upwork\API\Client;
use Illuminate\Http\Request;
use Upwork\API\Routers\Organization\Users;
use App\Models\User;
//use Upwork\API\Routers\Messages;
//use Upwork\API\Config;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function authenticate(Request $request)
    {
        $users = new Users($this->client);
        if (empty($users->getMyInfo()->user)) {
            exit;
        }
        $user = $users->getMyInfo()->user;

        $currentUser = User::where('reference', $user->reference)->count();
        if (!$currentUser && isset($_SESSION['access_token']) && isset($_SESSION['access_secret'])) {
            User::create([
                'reference' => $user->reference,
                'user_id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'public_url' => $user->public_url,
                'access_token' => $_SESSION['access_token'],
                'access_secret' => $_SESSION['access_secret'],
            ]);
            $userMessage = "User '$user->id' successfully added and log in!";
        //} elseif ($request->get('oauth_token')) {
        //    return $this->messages();
        } else {
            //$userMessage = "This user has already been added!";
            $userMessage = "User '$user->id' successfully log in!";
        }

        return view('welcome', [
            'userMessage' => $userMessage,
        ]);
    }
/*
    public function clear()
    {
        foreach ($_SESSION as $k => $v) {
            unset($_SESSION[$k]);
        }
        //unset($_SESSION['access_token']);
        //unset($_SESSION['access_secret']);

        return view('welcome', [
            'userMessage' => "Session clear! Need to log out from account on <a href=\"https://www.upwork.com\" target=\"_blank\">https://www.upwork.com</a>",
        ]);
    }

    public function restartClient($accessToken, $accessSecret)
    {
        $config = new Config([
            'accessToken'       => $accessToken,
            'accessSecret'      => $accessSecret,
        ]);

        $this->client = new Client($config);
    }

    public function messages($userMessage = '')
    {
        $users = User::get();
        foreach ($users as $user) {
            $this->restartClient($user->access_token, $user->access_secret);

            $messages = new Messages($this->client);
            $params = array(
                'limit' => 10,
            );

            for ($i = 0; $i < 3; $i++) {
                $rooms = $messages->getRooms($user->user_id, $params);
                if (!empty($rooms) && !isset($rooms->error)) {
                    break;
                }
            }

            if (!empty($rooms) && !isset($rooms->error)) {
                foreach ($rooms->rooms as $room) {
                    if ($room->numUnread) {
                        $messagesArray[] = [
                            'user' => $user->user_id,
                            'room_name' => $room->roomName,
                            'topic' => $room->topic,
                            'num_unread' => $room->numUnread,
                        ];
                    }
                }
            } else {
                $errorMessage = "Connection to $user->user_id crushed!";
                $errorsArray[] = $errorMessage;
            }
        }

        return view('welcome', [
            'userMessage' => $userMessage,
            'messages' => isset ($messagesArray) ? $messagesArray : null,
            'errors' => isset ($errorsArray) ? $errorsArray : null,
        ]);
    }

    public function messagesJson($userMessage = '')
    {
        $json = json_encode([
            'userMessage' => $userMessage,
            'messages' => $messagesArray,
        ]);

        return ['response' => $json];
    }
*/
}
