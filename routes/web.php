<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/callback', 'Controller@authenticate');
Route::get('/messages_json', 'Controller@messagesJson');

Route::get('/clear', 'NotificationsController@clearSession');
Route::get('/users', 'NotificationsController@getUsers');
Route::get('/messages', 'NotificationsController@getMessages');
Route::post('/token', 'NotificationsController@saveToken');