<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Upwork tool</title>

        <link rel="manifest" href="https://upwork.ikantam.com/manifest.json">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 54px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 20px;
            }
        </style>
    </head>
    <body>
        <div class="flex-top position-ref">
            <div class="content">
                <div class="title m-b-md">
                    Upwork tool
                </div>
                <div class="links">
                    <!--<a href="http://upwork.ikantam.com/clear">Clear session</a>
                    <a href="http://upwork.ikantam.com/callback">Add user</a>
                    <a href="http://upwork.ikantam.com/messages">Get messages</a>-->
                    <a href="{{url('/callback')}}">Log in / Add user</a>
                    <a href="{{url('/users')}}">Show users</a>
                    <a href="{{url('/messages')}}">Get messages</a>
                    <a href="{{url('/clear')}}">Log out</a>
                    <!--<input type="button" id="messages" value="Get messages"/>-->
                </div>
                <p><button type="button" id="subscribe">Subscribe to push notifications</button></p>
            </div>
        </div>
        @if (isset($users))
            @foreach ($users as $user)
                <p>{{$user}}</p>
            @endforeach
        @endif
        @if (!empty($userMessage))
            <p><font color="green">{!!$userMessage!!}</font></p>
        @endif
        @if (isset($messages))
            @foreach ($messages as $message)
                <p>User '{{$message['user']}}' - {{$message['num_unread']}} unread message in room '{{$message['room_name']}}' with topic '{{$message['topic']}}'</p>
            @endforeach
        @endif
        @if (isset($errors))
            @foreach ($errors as $error)
                <p><font color="red">{{$error}}</font></p>
            @endforeach
        @endif
    </body>
</html>

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script>
    /*$('#messages').on('click', function() {
        $.ajax({
            url: 'http://upwork-api.loc/messages_json',
            //data: $('#form-report').serialize(),
            dataType: 'json',
            type: 'get',
            success: function (data) {
                var resp = JSON.parse(data.response);
                console.log(resp.messages[0].topic);
            }
        });
    });*/
</script>

<script type="text/javascript" src="//www.gstatic.com/firebasejs/3.6.8/firebase.js"></script>
<script type="text/javascript" src="https://upwork.ikantam.com/firebase_subscribe.js"></script>